//Tyler Mison
//1511539

package code;

public class BikeStore {
    public static void main(String[] args) {

        // Create a Bicycle[] to hold 4 Bicycle objects
        Bicycle[] bikes = new Bicycle[4];

        // Initialize the 4 spots of the array to hold 4 different Bicycle objects
        bikes[0] = new Bicycle("Cannondale Bad Boy 3", 8, 30.0);
        bikes[1] = new Bicycle("Giant Escape 1", 36, 37.0);
        bikes[2] = new Bicycle("Trek FX Sport 6", 11, 32.0);
        bikes[3] = new Bicycle("Specialized Turbo Vado", 11, 45.0);

        // Loop through the entire Bicycle[] and print the results returned by the toString() method
        for (Bicycle bike : bikes) {
            System.out.println(bike);
        }
    }
}

