//Tyler Mison
//1511539

package code;

public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return "Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", MaxSpeed: " + maxSpeed;
    }
}